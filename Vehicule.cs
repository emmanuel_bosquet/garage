using System;

namespace Garage
{
    public abstract class Vehicule
    {
        public string Plaque { get; protected set; }
        public string Marque { get; protected set; }
        public string Modele { get; protected set; }
        public int NombreDeRoues { get; protected set; }
        public int NombreDeLitres { get; protected set; }
        public int Prix { get; protected set; }

        // cette méthode doit être implémentée dans chaque instance fille
        public abstract void Rouler();
        public abstract void Freiner();
        public abstract void Accelerer();
        
        public void Klaxonner()
        {
            Console.WriteLine("Tut TUUUUUUUUUUT");
        }

        public void Immatriculer(string numero)
        {
            Plaque = numero;
        }
        public int GetNombreLitres()
        {
            return NombreDeLitres;
        }

        public void SetNombreLitres(int nombre)
        {
            NombreDeLitres = nombre;
        }

        // constructeur
        public Vehicule(string plaque)
        {
            Immatriculer(plaque);
        }
    }
}