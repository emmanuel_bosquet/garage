# Mon garage

Ceci est un exercice de cours pour aborder la programmation orientée objet. Nous avons créé une classe abstraite qui regroupe tous les véhicules, et des classes concrètes telles que celle-ci :

```cs
using System;

namespace Garage
{
    public class Voiture : Personnel
    {
        private int _nombreRoues;
        private int _tonnage;

        public Voiture(string plaque) : base(plaque) 
        { }

        public override void Rouler()
        {
            Console.WriteLine("La voiture roule !");
        }

        public override void Freiner()
        {
            Console.WriteLine("La voiture freine !");
        }

        public override void Accelerer()
        {
            Console.WriteLine("La voiture accélère !");
        }
    }
}
```

