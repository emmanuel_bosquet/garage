using System;

namespace Garage
{
    public class Moto : Personnel
    {
        private int _nombreRoues;
        private int _tonnage;

        public Moto(string plaque) : base(plaque)
        { }

        public override void Rouler()
        {
            Console.WriteLine("La moto roule !");
        }

        public override void Freiner()
        {
            Console.WriteLine("La moto freine !");
        }

        public override void Accelerer()
        {
            Console.WriteLine("La moto accélère !");
        }
    }
}