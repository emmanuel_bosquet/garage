using System;

namespace Garage {
    public abstract class Personnel : Vehicule 
    {
        public int NombreDePassager { get; protected set; }

        // base() appelle le constructeur de la classe mère
        // et lui passe le paramètre
        public Personnel (string plaque) : base (plaque) 
        { }
    }
}